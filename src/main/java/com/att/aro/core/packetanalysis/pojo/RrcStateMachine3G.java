/*
 *  Copyright 2017 AT&T
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package com.att.aro.core.packetanalysis.pojo;

import com.google.common.base.Verify;
import com.mobileenerlytics.symbolic.MyDouble;

public class RrcStateMachine3G extends AbstractRrcStateMachine {
	private double idleTime;
	private MyDouble idleEnergy;
	private double dchTime;
	private MyDouble dchEnergy;
	private MyDouble dchTailEnergy;
	private double fachTime;
	private MyDouble fachEnergy;
	private double fachTailTime;
	private MyDouble fachTailEnergy;
	private double idleToDch;
	private double idleToDchTime;
	private MyDouble idleToDchEnergy;
	private double fachToDch;
	private double fachToDchTime;
	private MyDouble fachToDchEnergy;
	private double dchTailTime;
	public double getIdleTime() {
		return idleTime;
	}
	public void setIdleTime(double idleTime) {
		this.idleTime = idleTime;
	}
	public MyDouble getIdleEnergy() {
		return idleEnergy;
	}
	public void setIdleEnergy(MyDouble idleEnergy) {
	    Verify.verify(idleEnergy.isUnmodifiable());
		this.idleEnergy = idleEnergy;
	}
	public double getDchTime() {
		return dchTime;
	}
	public void setDchTime(double dchTime) {
		this.dchTime = dchTime;
	}
	public MyDouble getDchEnergy() {
		return dchEnergy;
	}
	public void setDchEnergy(MyDouble dchEnergy) {
		Verify.verify(dchEnergy.isUnmodifiable());
		this.dchEnergy = dchEnergy;
	}
	public MyDouble getDchTailEnergy() {
		return dchTailEnergy;
	}
	public void setDchTailEnergy(MyDouble dchTailEnergy) {
		Verify.verify(dchTailEnergy.isUnmodifiable());
		this.dchTailEnergy = dchTailEnergy;
	}
	public double getFachTime() {
		return fachTime;
	}
	public void setFachTime(double fachTime) {
		this.fachTime = fachTime;
	}
	public MyDouble getFachEnergy() {
		return fachEnergy;
	}
	public void setFachEnergy(MyDouble fachEnergy) {
		Verify.verify(fachEnergy.isUnmodifiable());
		this.fachEnergy = fachEnergy;
	}
	public double getFachTailTime() {
		return fachTailTime;
	}
	public void setFachTailTime(double fachTailTime) {
		this.fachTailTime = fachTailTime;
	}
	public MyDouble getFachTailEnergy() {
		return fachTailEnergy;
	}
	public void setFachTailEnergy(MyDouble fachTailEnergy) {
		Verify.verify(fachTailEnergy.isUnmodifiable());
		this.fachTailEnergy = fachTailEnergy;
	}
	public double getIdleToDch() {
		return idleToDch;
	}
	public void setIdleToDch(double idleToDch) {
		this.idleToDch = idleToDch;
	}
	public double getIdleToDchTime() {
		return idleToDchTime;
	}
	public void setIdleToDchTime(double idleToDchTime) {
		this.idleToDchTime = idleToDchTime;
	}
	public MyDouble getIdleToDchEnergy() {
		return idleToDchEnergy;
	}
	public void setIdleToDchEnergy(MyDouble idleToDchEnergy) {
		Verify.verify(idleToDchEnergy.isUnmodifiable());
		this.idleToDchEnergy = idleToDchEnergy;
	}
	public double getFachToDch() {
		return fachToDch;
	}
	public void setFachToDch(double fachToDch) {
		this.fachToDch = fachToDch;
	}
	public double getFachToDchTime() {
		return fachToDchTime;
	}
	public void setFachToDchTime(double fachToDchTime) {
		this.fachToDchTime = fachToDchTime;
	}
	public MyDouble getFachToDchEnergy() {
		return fachToDchEnergy;
	}
	public void setFachToDchEnergy(MyDouble fachToDchEnergy) {
	    Verify.verify(fachToDchEnergy.isUnmodifiable());
		this.fachToDchEnergy = fachToDchEnergy;
	}
	public double getDchTailTime() {
		return dchTailTime;
	}
	public void setDchTailTime(double dchTailTime) {
		this.dchTailTime = dchTailTime;
	}

	/**
	 * Returns the ratio of total DCH time to the total trace duration.
	 * 
	 * @return The DCH time ratio value.
	 */
	public double getDchTimeRatio() {
		return super.getTraceDuration() != 0.0 ? dchTime / getTraceDuration() : 0.0;
	}
	/**
	 * Returns the ratio of total FACH time to the total trace duration. 
	 * 
	 * @return The FACH time ratio value.
	 */
	public double getFachTimeRatio() {
		return getTraceDuration() != 0.0 ? fachTime / getTraceDuration() : 0.0;
	}
	/**
	 * Returns the ratio of total IDLE state time to the total trace duration.
	 * 
	 * @return  The IDLE time ratio value.
	 */
	public double getIdleTimeRatio() {
		return getTraceDuration() != 0.0 ? idleTime / getTraceDuration() : 0.0;
	}
	/**
	 * Returns the ratio of total IDLE to DCH time, to the trace duration.
	 * 
	 * @return The IDLE to DCH time ratio value.
	 */
	public double getIdleToDchTimeRatio() {
		return getTraceDuration() != 0.0 ? idleToDchTime / getTraceDuration() : 0.0;
	}
	/**
	 * Returns the ratio of total FACH to DCH time, to the trace duration.
	 * 
	 * @return The FACH to DCH time ratio value.
	 */
	public double getFachToDchTimeRatio() {
		return getTraceDuration() != 0.0 ? fachToDchTime / getTraceDuration() : 0.0;
	}
	/**
	 * Returns the ratio of total DCH tail time to the total trace duration.
	 * 
	 * @return The DCH tail ratio value.
	 */
	public double getDchTailRatio() {
		return dchTime != 0.0 ? dchTailTime / dchTime : 0.0;
	}
	/**
	 * Returns the ratio of total FACH tail time to the total trace duration. 
	 * 
	 * @return The FACH tail ratio value.
	 */
	public double getFachTailRatio() {
		return fachTime != 0.0 ? fachTailTime / fachTime : 0.0;
	}
	/**
	 * Returns the ratio of the total amount of promotion time to the trace duration. 
	 * 
	 * @return The total promotion ratio value.
	 */
	public double getPromotionRatio() {
		return getPacketsDuration() != 0.0 ? (idleToDchTime + fachToDchTime) / getPacketsDuration() : 0.0;
	}
	@Override
	public RrcStateMachineType getType() {
		return RrcStateMachineType.Type3G;
	}
}
