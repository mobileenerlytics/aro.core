/*
 *  Copyright 2014 AT&T
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package com.att.aro.core.packetanalysis.impl;

import com.att.aro.core.configuration.IProfileFactory;
import com.att.aro.core.configuration.pojo.*;
import com.att.aro.core.packetanalysis.IRrcStateMachineFactory;
import com.att.aro.core.packetanalysis.IRrcStateRangeFactory;
import com.att.aro.core.packetanalysis.pojo.*;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Date: November 3, 2014
 */
public class RrcStateMachineFactoryImpl implements IRrcStateMachineFactory {

	@Autowired
	private IRrcStateRangeFactory staterange;
	@Autowired
	private IProfileFactory profilefactory;
	@Autowired
	private DoubleFactory doubleFactory;
	
	@Override
	public AbstractRrcStateMachine create(List<PacketInfo> packetlist,
			Profile profile, double packetDuration, double traceDuration, double totalBytes,
			TimeRange timerange) {
		List<RrcStateRange> staterangelist = staterange.create(packetlist, profile, traceDuration);
		if(timerange != null){
			staterangelist = this.getRRCStatesForTheTimeRange(staterangelist, timerange.getBeginTime(), timerange.getEndTime());
		}
		AbstractRrcStateMachine data = null;
		if(profile.getProfileType() == ProfileType.T3G){
			data = run3GRRcStatistics(staterangelist, (Profile3G)profile, totalBytes, packetDuration, traceDuration);
		}else if(profile.getProfileType() == ProfileType.LTE){
			data = runLTERRcStatistics(staterangelist, (ProfileLTE)profile, packetlist, totalBytes, packetDuration, traceDuration);
		}else if(profile.getProfileType() == ProfileType.WIFI){
			data = runWiFiRRcStatistics(staterangelist, (ProfileWiFi)profile, totalBytes, packetDuration, traceDuration);
		}
		if(data != null){
			data.setStaterangelist(staterangelist);
		}
		return data;
	}
	private RrcStateMachineWiFi runWiFiRRcStatistics(List<RrcStateRange> staterangelist, ProfileWiFi prof,
			double totalBytes, double packetDuration, double traceDuration) {
		MyDouble totalRRCEnergy = doubleFactory.newSymbolicDouble(0);
		double wifiActiveTime = 0, wifiIdleTime = 0, wifiTailTime = 0;
		MyDouble wifiTailEnergy = doubleFactory.newSymbolicDouble(0);
		MyDouble wifiActiveEnergy = doubleFactory.newSymbolicDouble(0);
		MyDouble wifiIdleEnergy = doubleFactory.newSymbolicDouble(0);
		for (RrcStateRange rrc : staterangelist) {
			double rrcTimeDiff = rrc.getEndTime() - rrc.getBeginTime();

			MyDouble energy = profilefactory.energyWiFi(rrc.getBeginTime(), rrc.getEndTime(), rrc.getState(), prof);
			rrc.setEnergy(energy);

			totalRRCEnergy.add(energy);
			switch (rrc.getState()) {
			case WIFI_ACTIVE:
				wifiActiveTime += rrcTimeDiff;
				wifiActiveEnergy.add(energy);
				break;
			case WIFI_TAIL:
				//wifiActiveTime += rrcTimeDiff;
				wifiActiveEnergy.add(energy);
				wifiTailTime += rrcTimeDiff;
				wifiTailEnergy.add(energy);
				break;
			case WIFI_IDLE:
				wifiIdleTime += rrcTimeDiff;
				wifiIdleEnergy.add(energy);
				break;
			default:
				break;
			}
        }
		totalRRCEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(totalRRCEnergy);
		wifiActiveEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(wifiActiveEnergy);
		wifiTailEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(wifiTailEnergy);
		wifiIdleEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(wifiIdleEnergy);
		MyDouble joulesPerKilobyte = totalBytes != 0 ? totalRRCEnergy.divide(totalBytes / 1000.0) : DoubleFactory.newUnmodifiableDouble(0);
		RrcStateMachineWiFi stmachine = new RrcStateMachineWiFi();
		
		stmachine.setJoulesPerKilobyte(joulesPerKilobyte);
		stmachine.setPacketsDuration(packetDuration);
		stmachine.setTotalRRCEnergy(totalRRCEnergy);
		stmachine.setTraceDuration(traceDuration);
		
		stmachine.setWifiActiveEnergy(wifiActiveEnergy);
		stmachine.setWifiActiveTime(wifiActiveTime);
		stmachine.setWifiIdleEnergy(wifiIdleEnergy);
		stmachine.setWifiIdleTime(wifiIdleTime);
		stmachine.setWifiTailTime(wifiTailTime);
		stmachine.setWifiTailEnergy(wifiTailEnergy);
		
		return stmachine;
	}
	/**
	 * LTE RRC state time modification.
	 */
	private RrcStateMachineLTE runLTERRcStatistics(List<RrcStateRange> staterangelist, ProfileLTE profile, List<PacketInfo> packets,
			double totalBytes, double packetsDuration, double traceDuration) {

	    MyDouble totalRRCEnergy = doubleFactory.newSymbolicDouble(0);
        MyDouble lteIdleEnergy = doubleFactory.newSymbolicDouble(0);
        MyDouble lteIdleToCRPromotionEnergy = doubleFactory.newSymbolicDouble(0);
        MyDouble lteDrxShortEnergy = doubleFactory.newSymbolicDouble(0);
        MyDouble lteDrxLongEnergy = doubleFactory.newSymbolicDouble(0);
        MyDouble lteCrEnergy = doubleFactory.newSymbolicDouble(0);
        MyDouble lteCrTailEnergy = doubleFactory.newSymbolicDouble(0);

		double lteIdleTime = 0, lteIdleToCRPromotionTime = 0;
		double lteCrTime = 0, lteCrTailTime = 0;
		double lteDrxShortTime = 0, lteDrxLongTime = 0;

		for (RrcStateRange rrc : staterangelist) {
			double duration = rrc.getEndTime() - rrc.getBeginTime();
			MyDouble energy = profilefactory.energyLTE(rrc.getBeginTime(), rrc.getEndTime(), rrc.getState(), profile, packets);
			totalRRCEnergy.add(energy);
			switch (rrc.getState()) {
			case LTE_IDLE:
				lteIdleTime += duration;
				lteIdleEnergy.add(energy);
				break;
			case LTE_PROMOTION:
				lteIdleToCRPromotionTime += duration;
				lteIdleToCRPromotionEnergy.add(energy);
				break;
			case LTE_CONTINUOUS:
				lteCrTime += duration;
				lteCrEnergy.add(energy);
				break;
			case LTE_CR_TAIL:
				lteCrTime += duration;
				lteCrTailTime += duration;
				lteCrEnergy.add(energy);
				lteCrTailEnergy.add(energy);
				break;
			case LTE_DRX_SHORT:
				lteDrxShortTime += duration;
				lteDrxShortEnergy.add(energy);
				break;
			case LTE_DRX_LONG:
				lteDrxLongTime += duration;
				lteDrxLongEnergy.add(energy);
				break;
			default:
				break;
			}
			rrc.setEnergy(energy);
		}

		totalRRCEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(totalRRCEnergy);
        lteIdleEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(lteIdleEnergy);
        lteIdleToCRPromotionEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(lteIdleToCRPromotionEnergy);
        lteDrxShortEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(lteDrxShortEnergy);
        lteDrxLongEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(lteDrxLongEnergy);
        lteCrEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(lteCrEnergy);
        lteCrTailEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(lteCrTailEnergy);
		double bytes = totalBytes;
		MyDouble joulesPerKilobyte = bytes != 0 ? totalRRCEnergy.divide(bytes / 1000.0) :
				DoubleFactory.newUnmodifiableDouble(0.0);
		RrcStateMachineLTE stmachine = new RrcStateMachineLTE();
		stmachine.setJoulesPerKilobyte(joulesPerKilobyte);
		
		stmachine.setLteCrEnergy(lteCrEnergy);
		stmachine.setLteCrTailEnergy(lteCrTailEnergy);
		stmachine.setLteCrTailTime(lteCrTailTime);
		stmachine.setLteCrTime(lteCrTime);
		
		stmachine.setLteDrxLongEnergy(lteDrxLongEnergy);
		stmachine.setLteDrxLongTime(lteDrxLongTime);
		stmachine.setLteDrxShortEnergy(lteDrxShortEnergy);
		stmachine.setLteDrxShortTime(lteDrxShortTime);
		
		stmachine.setLteIdleEnergy(lteIdleEnergy);
		stmachine.setLteIdleTime(lteIdleTime);
		stmachine.setLteIdleToCRPromotionEnergy(lteIdleToCRPromotionEnergy);
		stmachine.setLteIdleToCRPromotionTime(lteIdleToCRPromotionTime);
		
		stmachine.setPacketsDuration(packetsDuration);
		stmachine.setTotalRRCEnergy(totalRRCEnergy);
		stmachine.setTraceDuration(traceDuration);
		
		return stmachine;
	}
	/**
	 * 3G RRC state time modification.
	 */
	private RrcStateMachine3G run3GRRcStatistics(List<RrcStateRange> staterangelist,
			Profile3G prof3g, double totalBytes, double packetsDuration, double traceDuration) {

		MyDouble idleEnergy = doubleFactory.newSymbolicDouble(0);
		MyDouble dchEnergy = doubleFactory.newSymbolicDouble(0);
		MyDouble dchTailEnergy = doubleFactory.newSymbolicDouble(0);
		MyDouble fachEnergy = doubleFactory.newSymbolicDouble(0);
		MyDouble fachTailEnergy = doubleFactory.newSymbolicDouble(0);
		MyDouble idleToDchEnergy = doubleFactory.newSymbolicDouble(0);
		MyDouble fachToDchEnergy = doubleFactory.newSymbolicDouble(0);

		double idleTime = 0, dchTime = 0, dchTailTime =0;
		double fachTime=0, fachTailTime=0;
		double idleToDch=0, idleToDchTime=0, fachToDch=0;
		double fachToDchTime=0;
		RrcStateMachine3G statemachine = new RrcStateMachine3G();
		
		for (RrcStateRange rrc : staterangelist) {
            MyDouble energy = profilefactory.energy3G(rrc.getBeginTime(), rrc.getEndTime(), rrc.getState(), prof3g);
			double duration = rrc.getEndTime() - rrc.getBeginTime();
			switch (rrc.getState()) {
			case STATE_IDLE:
				idleTime += duration;
				idleEnergy.add(energy);
				break;
			case STATE_DCH:
				dchTime += duration;
				dchEnergy.add(energy);
				break;
			case TAIL_DCH:
				//dchTime += duration;
				dchTailTime += duration;
				dchEnergy.add(energy);
				dchTailEnergy.add(energy);
				break;
			case STATE_FACH:
				fachTime += duration;
				fachEnergy.add(energy);
				break;
			case TAIL_FACH:
				fachTime += duration;
				fachTailTime += duration;
				fachTailEnergy.add(energy);
				fachEnergy.add(energy);
				break;
			case PROMO_IDLE_DCH:
				idleToDch++;
				idleToDchTime += duration;
				idleToDchEnergy.add(energy);
				break;
			case PROMO_FACH_DCH:
				fachToDch++;
				fachToDchTime += duration;
				fachToDchEnergy.add(energy);
				break;
			default:
				break;
			}
		}

		idleEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(idleEnergy);
		dchEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(dchEnergy);
		dchTailEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(dchTailEnergy);
		fachEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(fachEnergy);
		fachTailEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(fachTailEnergy);
		idleToDchEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(idleToDchEnergy);
		fachToDchEnergy = doubleFactory.cloneSymbolicUnmodifiableDouble(fachToDchEnergy);

		MyDouble totalRRCEnergy = fachEnergy.add(dchEnergy ).add(fachToDchEnergy).add(idleToDchEnergy)
				.add(idleEnergy);
		MyDouble joulesPerKilobyte = totalBytes != 0 ? totalRRCEnergy.divide(totalBytes / 1000.0) :
				DoubleFactory.newUnmodifiableDouble(0.0);
		
		statemachine.setDchEnergy(dchEnergy);
		statemachine.setDchTailEnergy(dchTailEnergy);
		statemachine.setDchTailTime(dchTailTime);
		statemachine.setDchTime(dchTime);
		
		statemachine.setFachEnergy(fachEnergy);
		statemachine.setFachTailEnergy(fachTailEnergy);
		statemachine.setFachTailTime(fachTailTime);
		statemachine.setFachTime(fachTime);
		statemachine.setFachToDch(fachToDch);
		statemachine.setFachToDchEnergy(fachToDchEnergy);
		statemachine.setFachToDchTime(fachToDchTime);
		
		statemachine.setIdleEnergy(idleEnergy);
		statemachine.setIdleTime(idleTime);
		statemachine.setIdleToDch(idleToDch);
		statemachine.setIdleToDchEnergy(idleToDchEnergy);
		statemachine.setIdleToDchTime(idleToDchTime);
		
		statemachine.setJoulesPerKilobyte(joulesPerKilobyte);
		statemachine.setPacketsDuration(packetsDuration);
		statemachine.setTotalRRCEnergy(totalRRCEnergy);
		statemachine.setTraceDuration(traceDuration);
		
		return statemachine;
	}
	private List<RrcStateRange> getRRCStatesForTheTimeRange(List<RrcStateRange> rrcRanges , double beginTime , double endTime){
		
		List<RrcStateRange> filteredRRCStates = new ArrayList<>();
		boolean stateAdded = false;

		for (RrcStateRange rrcRange : rrcRanges) {

			if (rrcRange.getBeginTime() >= beginTime
					&& rrcRange.getEndTime() <= endTime) {
				filteredRRCStates.add(rrcRange);
			} else if (rrcRange.getBeginTime() <= beginTime
					&& rrcRange.getEndTime() <= endTime && rrcRange.getEndTime() > beginTime) {
				filteredRRCStates.add(new RrcStateRange(beginTime, rrcRange
						.getEndTime(), rrcRange.getState()));
			} else if (rrcRange.getBeginTime() <= beginTime
					&& rrcRange.getEndTime() >= endTime) {
				filteredRRCStates.add(new RrcStateRange(beginTime, endTime,
						rrcRange.getState()));
			} else if (rrcRange.getBeginTime() >= beginTime && rrcRange.getBeginTime() < endTime
					&& rrcRange.getEndTime() >= endTime && !stateAdded) {
				filteredRRCStates.add(new RrcStateRange(rrcRange
						.getBeginTime(), endTime, rrcRange
						.getState()));
				stateAdded = true;
			}
		}
		return filteredRRCStates;
		
	}

}
