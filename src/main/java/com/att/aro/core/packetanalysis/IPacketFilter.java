package com.att.aro.core.packetanalysis;

import com.att.aro.core.packetanalysis.pojo.AnalysisFilter;
import com.att.aro.core.packetanalysis.pojo.PacketInfo;

import java.util.List;

public interface IPacketFilter {
	List<PacketInfo> filterPackets(AnalysisFilter filter, List<PacketInfo> packetsInfo);
}
