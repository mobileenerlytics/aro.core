package com.att.aro.core.packetanalysis.impl;

import com.att.aro.core.packetanalysis.IPacketFilter;
import com.att.aro.core.packetanalysis.pojo.AnalysisFilter;
import com.att.aro.core.packetanalysis.pojo.PacketInfo;
import com.att.aro.core.packetanalysis.pojo.TimeRange;
import com.att.aro.core.packetreader.pojo.UDPPacket;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.util.ArrayList;
import java.util.List;

public class PacketFilterImpl implements IPacketFilter {
	/**
	 * Runs the filtering process on the specified packets/PacketInfos.
	 *
	 * @return packets/PacketInfos filtered
	 */
	@Override
	public List<PacketInfo> filterPackets(AnalysisFilter filter, List<PacketInfo> packetsInfo) {
		if(filter == null)
			return packetsInfo;

		List<PacketInfo> filteredPackets = new ArrayList<PacketInfo>();	// create new packets according to the filter setting
		TimeRange timeRange = filter.getTimeRange();
		int packetIdx = 0;

		boolean ipv4Flag = false;
		boolean ipv6Flag = false;
		boolean udpFlag = false;

		if(!(filter.isIpv4Sel() && filter.isIpv6Sel() && filter.isUdpSel())){

			if(!(filter.isIpv4Sel() && filter.isIpv6Sel())){

				if(!filter.isIpv4Sel()){
					ipv4Flag = true;
				}
				if(!filter.isIpv6Sel()){
					ipv6Flag = true;
				}
			}
			if(!filter.isUdpSel()){
				udpFlag = true;
			}

		}

		for (PacketInfo packetInfo : packetsInfo) {
			if(ipv4Flag && packetInfo.getRemoteIPAddress() instanceof Inet4Address){
				continue;
			}
			if(ipv6Flag && packetInfo.getRemoteIPAddress() instanceof Inet6Address){
				continue;
			}
			if(udpFlag && packetInfo.getPacket() instanceof UDPPacket){
				continue;
			}

			// Check time range
			double timestamp = packetInfo.getTimeStamp();
			if (timeRange != null
						&& (timeRange.getBeginTime() > timestamp || timeRange.getEndTime() < timestamp)) {
					// Not in time range
					continue;
			}
			// Check to see if application is selected
			if (filter.getPacketColor(packetInfo) == null) {
					// App unknown by filter
				continue;
			}

			packetInfo.setPacketId(++packetIdx);
			filteredPackets.add(packetInfo);
		}

		return filteredPackets;
	}
}
