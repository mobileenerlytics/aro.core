package com.att.aro.mobe;

import com.att.aro.core.BaseTest;
import com.att.aro.core.configuration.pojo.ProfileType;
import com.att.aro.core.configuration.pojo.ProfileWiFi;
import com.att.aro.core.packetanalysis.IRrcStateRangeFactory;
import com.att.aro.core.packetanalysis.pojo.PacketInfo;
import com.att.aro.core.packetanalysis.pojo.RRCState;
import com.att.aro.core.packetanalysis.pojo.RrcStateRange;
import com.att.aro.core.packetreader.pojo.Packet;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class WifiRrcStateRangeFactoryTest extends BaseTest {
    private static final double ACTIVE_POWER = 100;
    private static final double TAIL_TIME_MS = 1800;
    private final long MS = 1000;

    @Autowired
    private IRrcStateRangeFactory rrcStateRangeFactory;

    private final ProfileWiFi profileWiFi;

    public WifiRrcStateRangeFactoryTest() {
        profileWiFi = spy(new ProfileWiFi());
        when(profileWiFi.getProfileType()).thenReturn(ProfileType.WIFI);
        when(profileWiFi.getWifiTailTime()).thenReturn(TAIL_TIME_MS);
        when(profileWiFi.getWifiActivePower()).thenReturn(new ConcreteUnmodifiableDouble(ACTIVE_POWER));
    }


    private PacketInfo getPacketInfo(double ts, int length) {
        Packet packet = mock(Packet.class);
        PacketInfo ret = spy(new PacketInfo(packet));
        when(ret.getTimeStamp()).thenReturn(ts);
        when(ret.getLen()).thenReturn(length);
        return ret;
    }

    @Test
    public void rrcStateRange_burst_same_len() {

        List<PacketInfo> packetlist = new ArrayList<>();
        double traceDuration = 1000.0;

        // Burst
        packetlist.add(getPacketInfo(MS, 1000));
        packetlist.add(getPacketInfo(MS + 500.0, 1000));
        packetlist.add(getPacketInfo(MS + 1500.0, 1000));

        // Burst
        packetlist.add(getPacketInfo(MS + 15000.0, 1000));

        List<RrcStateRange> testList = rrcStateRangeFactory.create(packetlist, profileWiFi, traceDuration);
        assertEquals(4, testList.size());

        // Each packet in first burst gets one third of active power, but has the same length as the tail
        RrcStateRange expectedTail = new RrcStateRange(MS + 1500, MS + 1500 + TAIL_TIME_MS, RRCState.WIFI_TAIL);

        // First packet of first burst
        List<RrcStateRange> actualList = packetlist.get(0).getStateRangeList();
        assertThat(actualList,
                hasItems(
                        new RrcStateRangeMatcher(new RrcStateRange(0, MS, RRCState.WIFI_IDLE)),
                        // Each packet in first burst gets one third of active energy.
                        // Active time = total active time/3 = 1500 / 3 = 500
                        new RrcStateRangeMatcher(new RrcStateRange(MS, MS + 500, RRCState.WIFI_ACTIVE)),
                        new RrcStateRangeMatcher(expectedTail)));

        // TAIL state's energy
        RrcStateRange tail = actualList.stream().filter(s -> s.getState() == RRCState.WIFI_TAIL).findFirst().get();
        myAssertEquals(TAIL_TIME_MS * ACTIVE_POWER / 3, tail.getEnergy(), 0.1);

        // Last packet of first burst
        actualList = packetlist.get(2).getStateRangeList();
        assertThat(actualList, hasItems(
                new RrcStateRangeMatcher(new RrcStateRange(MS + 1000, MS + 1500, RRCState.WIFI_ACTIVE)),
                new RrcStateRangeMatcher(expectedTail)));
        tail = actualList.stream().filter(s -> s.getState() == RRCState.WIFI_TAIL).findFirst().get();
        myAssertEquals(TAIL_TIME_MS * ACTIVE_POWER / 3, tail.getEnergy(), 0.1);
    }

    @Test
    public void rrcStateRange_burst_different_len() {
        List<PacketInfo> packetlist = new ArrayList<>();
        double traceDuration = 1000.0;

        // Burst
        packetlist.add(getPacketInfo(MS, 100));
        packetlist.add(getPacketInfo(MS + 500.0, 400));
        packetlist.add(getPacketInfo(MS + 1500.0, 1000));

        // Burst
        packetlist.add(getPacketInfo(MS + 15000.0, 1000));

        List<RrcStateRange> testList = rrcStateRangeFactory.create(packetlist, profileWiFi, traceDuration);
        assertEquals(4, testList.size());

        // Each packet in first burst gets one third of active power, but has the same length as the tail
        RrcStateRange expectedTail = new RrcStateRange(MS + 1500, MS + 1500 + TAIL_TIME_MS, RRCState.WIFI_TAIL);

        // First packet of first burst

        List<RrcStateRange> actualList = packetlist.get(0).getStateRangeList();
        assertThat(actualList, hasItems(
                // Packet in first burst gets energy proportion according to number of bytes
                new RrcStateRangeMatcher(new RrcStateRange(0, MS, RRCState.WIFI_IDLE)),
                // Active time = total active time/15 = 1500 / 15 = 100
                new RrcStateRangeMatcher(new RrcStateRange(MS, MS + 100, RRCState.WIFI_ACTIVE)),
                new RrcStateRangeMatcher(expectedTail)
        ));

        // TAIL state's energy
        RrcStateRange tail = actualList.stream().filter(s -> s.getState() == RRCState.WIFI_TAIL).findFirst().get();
        myAssertEquals(TAIL_TIME_MS * ACTIVE_POWER / 15, tail.getEnergy(), 0.1);

        actualList = packetlist.get(2).getStateRangeList();
        assertThat(actualList, hasItems(
                // Active time = total active time*2/3 = 1500 * 2/3 = 1000
                new RrcStateRangeMatcher(new RrcStateRange(MS + 500, MS + 1500, RRCState.WIFI_ACTIVE)),
                new RrcStateRangeMatcher(expectedTail)
        ));
        tail = actualList.stream().filter(s -> s.getState() == RRCState.WIFI_TAIL).findFirst().get();
        myAssertEquals(TAIL_TIME_MS * ACTIVE_POWER * 2 / 3, tail.getEnergy(), 0.1);
    }
}
