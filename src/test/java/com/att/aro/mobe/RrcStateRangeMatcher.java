package com.att.aro.mobe;

import com.att.aro.core.packetanalysis.pojo.RRCState;
import com.att.aro.core.packetanalysis.pojo.RrcStateRange;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

public class RrcStateRangeMatcher extends TypeSafeDiagnosingMatcher<RrcStateRange> {
    private final Matcher<? super Double> beginTime;
    private final Matcher<? super Double> endTime;
    private final Matcher<? super RRCState> state;
    private final RrcStateRange rrcStateRange;


    RrcStateRangeMatcher(RrcStateRange rrcStateRange) {
        this.rrcStateRange = rrcStateRange;
        beginTime = is(equalTo(rrcStateRange.getBeginTime()));
        endTime = is(equalTo(rrcStateRange.getEndTime()));
        state = is(equalTo(rrcStateRange.getState()));
    }

    public boolean matchesSafely(RrcStateRange o, Description mismatchDescription) {
        mismatchDescription.appendText("{");
        RrcStateRange range = (RrcStateRange) o;
        boolean matches = true;
        if(! beginTime.matches(range.getBeginTime())) {
            reportMismatch("beginTime", beginTime, range.getBeginTime(), mismatchDescription, matches);
            matches = false;
        }
        if(! endTime.matches(range.getEndTime())) {
            reportMismatch("endTime", endTime, range.getEndTime(), mismatchDescription, matches);
            matches = false;
        }
        if(! state.matches(range.getState())) {
            reportMismatch("state", state, range.getState(), mismatchDescription, matches);
            matches = false;
        }
        mismatchDescription.appendText("}");
        return matches;
    }

    public void describeTo(Description desc) {
        desc.appendText("{beginTime is ")
                .appendValue(rrcStateRange.getBeginTime())
                .appendText(", endTime is ")
                .appendValue(rrcStateRange.getEndTime())
                .appendText(", state ")
                .appendValue(rrcStateRange.getState())
                .appendText("}");
    }

    static void reportMismatch(String name, Matcher<?> matcher, Object item, Description mismatchDescription, boolean firstMismatch) {
        if (!firstMismatch)
        {
            mismatchDescription.appendText(", ");
        }
        mismatchDescription.appendText(name).appendText(" ");
        matcher.describeMismatch(item, mismatchDescription);
    }
}
