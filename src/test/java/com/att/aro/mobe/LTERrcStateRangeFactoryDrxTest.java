package com.att.aro.mobe;

import com.att.aro.core.BaseTest;
import com.att.aro.core.configuration.IProfileFactory;
import com.att.aro.core.configuration.pojo.ProfileLTE;
import com.att.aro.core.configuration.pojo.ProfileType;
import com.att.aro.core.packetanalysis.impl.RrcStateRangeFactoryImpl;
import com.att.aro.core.packetanalysis.pojo.PacketInfo;
import com.att.aro.core.packetanalysis.pojo.RRCState;
import com.att.aro.core.packetanalysis.pojo.RrcStateRange;
import com.att.aro.core.packetreader.pojo.Packet;
import com.mobileenerlytics.symbolic.ConcreteDoubleFactory;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.hamcrest.StringDescription;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static junit.framework.TestCase.*;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.AdditionalAnswers.returnsArgAt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

public class LTERrcStateRangeFactoryDrxTest extends BaseTest {
    private static final double ENERGY = 1500;
    @Mock
    IProfileFactory profileFactory;

    @InjectMocks @Spy
    RrcStateRangeFactoryImpl rrcStateRangeFactory;

    @Spy
    ProfileLTE profileLTE;

    @Spy @InjectMocks
    DoubleFactory doubleFactory;

    @Spy
    ConcreteDoubleFactory iDoubleFactory;

    final long MS = 1000;

    private static final double PROMOTION_TIME = 100;
    private static final double INACTIVITY_TIMER = 100;
    private static final double DRX_SHORT_TIME = 100;
    private static final double DRX_PING_TIME = 100;
    private static final double DRX_LONG_TIME = 100;
    private static final double IDLE_PING_TIME = 100;
    private static final double DRX_SHORT_PING_PERIOD = 100;
    private static final double DRX_LONG_PING_PERIOD = 100;
    private static final double IDLE_PING_PERIOD = 100;

    private static final double LTE_PROMOTION_POWER = 100;
    private static final double DRX_SHORT_PING_POWER = 100;
    private static final double DRX_LONG_PING_POWER = 100;
    private static final double LTE_TAIL_POWER = 100;
    private static final double LTE_IDLE_PING_POWER = 100;
    private static final double LTE_IDLE_POWER = 100;

    private static final double LTE_ALPHA_UP = 100;
    private static final double LTE_ALPHA_DOWN = 100;
    private static final double LTE_BETA = 100;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(profileLTE.getProfileType()).thenReturn(ProfileType.LTE);

        when(profileLTE.getPromotionTime()).thenReturn(PROMOTION_TIME);
        when(profileLTE.getInactivityTimer()).thenReturn(INACTIVITY_TIMER);
        when(profileLTE.getDrxShortTime()).thenReturn(DRX_SHORT_TIME);
        when(profileLTE.getDrxPingTime()).thenReturn(DRX_PING_TIME);
        when(profileLTE.getDrxLongTime()).thenReturn(DRX_LONG_TIME);
        when(profileLTE.getIdlePingTime()).thenReturn(IDLE_PING_TIME);

        when(profileLTE.getDrxShortPingPeriod()).thenReturn(DRX_SHORT_PING_PERIOD);
        when(profileLTE.getDrxLongPingPeriod()).thenReturn(DRX_LONG_PING_PERIOD);
        when(profileLTE.getIdlePingPeriod()).thenReturn(IDLE_PING_PERIOD);

        when(profileLTE.getLtePromotionPower()).thenReturn(new ConcreteUnmodifiableDouble(LTE_PROMOTION_POWER));
        when(profileLTE.getDrxShortPingPower()).thenReturn(new ConcreteUnmodifiableDouble(DRX_SHORT_PING_POWER));
        when(profileLTE.getDrxLongPingPower()).thenReturn(new ConcreteUnmodifiableDouble(DRX_LONG_PING_POWER));
        when(profileLTE.getLteTailPower()).thenReturn(new ConcreteUnmodifiableDouble(LTE_TAIL_POWER));
        when(profileLTE.getLteIdlePingPower()).thenReturn(new ConcreteUnmodifiableDouble(LTE_IDLE_PING_POWER));
        when(profileLTE.getLteIdlePower()).thenReturn(new ConcreteUnmodifiableDouble(LTE_IDLE_POWER));

        when(profileLTE.getLteAlphaUp()).thenReturn(new ConcreteUnmodifiableDouble(LTE_ALPHA_UP));
        when(profileLTE.getLteAlphaDown()).thenReturn(new ConcreteUnmodifiableDouble(LTE_ALPHA_DOWN));
        when(profileLTE.getLteBeta()).thenReturn(new ConcreteUnmodifiableDouble(LTE_BETA));

        when(profileFactory.energyNetwork(anyObject(), anyObject(), anyObject())).thenReturn(new ConcreteUnmodifiableDouble(ENERGY));

        doAnswer(returnsArgAt(2))
                .when(rrcStateRangeFactory)
                .promoteLTE(anyObject(), anyDouble(), anyDouble(), anyObject());

        doAnswer(
                new Answer() {
                    public Object answer(InvocationOnMock invocation) {
                        RrcStateRangeFactoryImpl object = (RrcStateRangeFactoryImpl) invocation.getMock();
                        Object[] args = invocation.getArguments();
                        List<PacketInfo> burst = invocation.getArgumentAt(0, List.class);
                        double firstInBurstTime = invocation.getArgumentAt(1, Double.class);
                        double lastInBurstTime = invocation.getArgumentAt(2, Double.class);
                        double end = invocation.getArgumentAt(3, Double.class);
                        ProfileLTE profile  = invocation.getArgumentAt(4, ProfileLTE.class);
                        return object.drxAfterActiveLTE(burst, lastInBurstTime, end, profile);
                    }}
        ).when(rrcStateRangeFactory).activeAndTailLTE(anyObject(), anyDouble(), anyDouble(), anyDouble(), anyObject());
    }

    private PacketInfo getPacketInfo(double ts, int length) {
        Packet packet = mock(Packet.class);
        PacketInfo ret = spy(new PacketInfo(packet));
        when(ret.getTimeStamp()).thenReturn(ts);
        when(ret.getLen()).thenReturn(length);
        return ret;
    }

    private MyDouble getEnergy(List<RrcStateRange> ranges, RRCState state) {
        return ranges.stream()
                .filter(r -> (r.getState() == state))
                .findFirst().get().getEnergy();
    }

    @Test
    public void testShortAndLongDRX_BurstDifferentLen() {
        List<PacketInfo> packetlist = new ArrayList<>();
        double traceDuration = 10000.0;

        // Burst of 1:2:3 length ratio
        packetlist.add(getPacketInfo(1000, 500));
        packetlist.add(getPacketInfo(1030, 1000));
        packetlist.add(getPacketInfo(1060, 1500));

        // Burst
        packetlist.add(getPacketInfo(2500, 1000));

        List<RrcStateRange> testList = rrcStateRangeFactory.create(packetlist, profileLTE, traceDuration);
        assertEquals(4, testList.size());

        List<List<RrcStateRange>> packetStateRanges = packetlist.stream().map(PacketInfo::getStateRangeList).collect(toList());

        /** FIRST BURST TESTING **/
        /* All three packets in burst will have same length tail but energy will be in 1:2:3 */
        {
            // DRX SHORT!
            RrcStateRangeMatcher expecteddrxShort =
                    new RrcStateRangeMatcher(new RrcStateRange(1060, 1060 + DRX_SHORT_TIME, RRCState.LTE_DRX_SHORT));
            double expectedDrxShortEnergy = ENERGY / 6;

            packetStateRanges.stream()
                    .limit(3)  // For this burst of three packets
                    .flatMap(r -> r.stream())
                    .filter(r -> r.getState() == RRCState.LTE_DRX_SHORT)
                    .forEach(
                            p -> {
                                StringDescription desc = new StringDescription();
                                if (!expecteddrxShort.matchesSafely(p, desc)) {
                                    System.out.println(desc.toString());
                                    fail();
                                }
                            }
                    );
            myAssertEquals(expectedDrxShortEnergy, getEnergy(packetStateRanges.get(0), RRCState.LTE_DRX_SHORT),
                    0.1);
            myAssertEquals(2 * expectedDrxShortEnergy, getEnergy(packetStateRanges.get(1), RRCState.LTE_DRX_SHORT),
                    0.1);
            myAssertEquals(3 * expectedDrxShortEnergy, getEnergy(packetStateRanges.get(2), RRCState.LTE_DRX_SHORT),
                    0.1);
        }

        {
            // DRX LONG!
            RrcStateRangeMatcher expecteddrxLong =
                    new RrcStateRangeMatcher(new RrcStateRange(1060 + DRX_SHORT_TIME, 1060 + DRX_SHORT_TIME + DRX_LONG_TIME, RRCState.LTE_DRX_LONG));
            double expectedDrxLongEnergy = ENERGY / 6;

            packetStateRanges.stream()
                    .limit(3)  // For this burst of three packets
                    .flatMap(r -> r.stream())
                    .filter(r -> r.getState() == RRCState.LTE_DRX_LONG)
                    .forEach(
                            p -> {
                                StringDescription desc = new StringDescription();
                                if (!expecteddrxLong.matchesSafely(p, desc)) {
                                    System.out.println(desc.toString());
                                    fail();
                                }
                            }
                    );
            myAssertEquals(expectedDrxLongEnergy, getEnergy(packetStateRanges.get(0), RRCState.LTE_DRX_LONG),
                    0.1);
            myAssertEquals(2 * expectedDrxLongEnergy, getEnergy(packetStateRanges.get(1), RRCState.LTE_DRX_LONG),
                    0.1);
            myAssertEquals(3 * expectedDrxLongEnergy, getEnergy(packetStateRanges.get(2), RRCState.LTE_DRX_LONG),
                    0.1);
        }



        /** FIRST BURST TESTING ENDS **/

        /** SECOND BURST TESTING **/
        assertThat(packetStateRanges.get(3), hasItems(
                new RrcStateRangeMatcher(
                        new RrcStateRange(2500, 2500 + DRX_SHORT_TIME, RRCState.LTE_DRX_SHORT)),
                new RrcStateRangeMatcher(
                        new RrcStateRange(2500 + DRX_SHORT_TIME, 2500 + DRX_SHORT_TIME + DRX_LONG_TIME, RRCState.LTE_DRX_LONG))
        ));
        /** SECOND BURST TESTING ENDS **/
    }
}
