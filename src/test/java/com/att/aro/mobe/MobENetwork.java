package com.att.aro.mobe;

import com.att.aro.core.BaseTest;
import com.att.aro.core.IAROService;
import com.att.aro.core.bestpractice.pojo.AbstractBestPracticeResult;
import com.att.aro.core.bestpractice.pojo.BPResultType;
import com.att.aro.core.configuration.IProfileFactory;
import com.att.aro.core.pojo.AROTraceData;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static com.att.aro.core.settings.SettingsUtil.retrieveBestPractices;
import static org.junit.Assert.assertTrue;

public class MobENetwork extends BaseTest {
    @Autowired
    private IAROService serv;

    @Autowired
    private IProfileFactory profileFactory;

    @Test
    public void network() throws IOException {
        String pcap = getClass().getResource("/capture.pcap").getFile();

        File pcapFile = new File(pcap);
        assertTrue(pcapFile.exists());

        String reportPath = Files.createTempFile("report-", ".json").toAbsolutePath().toString();
        System.out.println(reportPath);
        AROTraceData traceData = serv.analyzeFile(retrieveBestPractices(), pcap, profileFactory.createWiFiFromDefaultResourceFile(), null);
        traceData.getBestPracticeResults().stream()
                .filter(r -> r.getResultType() == BPResultType.WARNING || r.getResultType() == BPResultType.FAIL)
                .map(r -> r.getResultType() + " " + r.getOverviewTitle() + " " + r.getDetailTitle())
                .forEach(System.out::println);
        serv.getJSonReport(reportPath, traceData);
    }
}
