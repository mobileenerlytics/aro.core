package com.att.aro.core.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;

import com.att.aro.core.settings.impl.SettingsImpl;
import org.mockito.ArgumentMatcher;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

public interface TestingUtil {
	
	static void SetFinalField(Class<SettingsImpl> classInst, String name, String value)
			throws NoSuchFieldException, IllegalAccessException {
		Field pathField = classInst.getDeclaredField(name);
		pathField.setAccessible(true);
		Field modifier = Field.class.getDeclaredField("modifiers");
		modifier.setAccessible(true);
		modifier.setInt(pathField, pathField.getModifiers() & ~Modifier.FINAL);
		pathField.set(null, value);
	}

	/**
	 * Custom matcher that checks if a list is equal to expected when elements don't have equals()
	 * using {@link ReflectionEquals}.
	 */
	static ArgumentMatcher<List> equalsList(List expectedList) {
		return new ArgumentMatcher<List>() {
			@Override
			public boolean matches(Object argument) {
				List list = (List) argument;
				boolean assertion;
				// Check size
				return list.size() == expectedList.size() &&
						containsAll(expectedList).matches(list) &&
						containsAll(list).matches(expectedList);
			}
		};
	}

	/**
	 * Custom matcher that checks if a list contains all the expected elements when they don't have
	 * equals() using {@link ReflectionEquals}.
	 */
	static ArgumentMatcher<List> containsAll(List expected) {
		return new ArgumentMatcher<List>() {
			@Override
			public boolean matches(Object argument) {
				List list = (List) argument;
				boolean assertion;
				for (Object expectedObject : expected) {
					ReflectionEquals reflectionEquals = new ReflectionEquals(expectedObject, null);
					assertion = false;
					for (Object object : list) {
						if (reflectionEquals.matches(object)) {
							assertion = true;
							break;
						}
					}
					if (!assertion) return false;
				}
				return true;
			}
		};
	}
}
